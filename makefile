all: paper.pdf

paper.pdf: *.tex paper.bib figures/*.pdf
	pdflatex paper
	bibtex paper
	pdflatex paper
	pdflatex paper
	pdflatex paper

clean:
	-@rm -f *~ *.bak *.out *.aux  *.bbl  *.blg  *.dvi  *.log  *.pdf
